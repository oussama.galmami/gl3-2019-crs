﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_1
{
    static class essai
    {
        static public String retireCar(this string ch, char c)
        {
            int i = ch.IndexOf(c);
            return ch.Remove(i, 1);
        }
    }
    public class Point
    {
        public int x { get; set; }
        public int y { get; set; }
        public void affiche()
        {
            Console.WriteLine(" variable x : " + this.x + " ");
        }

    }
    class Calcul
    {
        public static int somme(int a, int b)
        {
            return a + b;
        }
        public static int produit(int a, int b)
        {
            return a * b;
        }
    }
    class Program
    {
        delegate int mult(int x);
        delegate int calcul(int x, int y);
        static void Main(string[] args)
        {
            /*partie1*/
            //Console.WriteLine("hello world!");
            var i = 0;
            Point p = new Point { x = 0, y = 1 };
            p.x = 1;
            p.y = 2;
            p.affiche();
            Console.WriteLine(" variable x : " + p.x + " ");
            /*Console.WriteLine(i + 1);
            Console.WriteLine(i.GetType());
            string ch = "world";
            Console.WriteLine(ch.cryptage());
            mult multiplication = somme;
            multiplication += x=> {
                Console.WriteLine("produit2");
                return x*x;
            } ;
            Console.WriteLine(multiplication(3));*/
            var personne1 = new { nom = "bbbb", prenom = "aaaa" };
            var personne2 = new { nom = "1111", prenom = "2222" };
            Console.WriteLine(personne1);
            Console.WriteLine(personne1.GetType());
            Console.WriteLine(personne2);
            Console.WriteLine(personne2.GetType());

            /*partie 2 delegate*/
            calcul c = Calcul.produit;

            int result = 0;
            Console.WriteLine("premier nb svp :");
            int n1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("deuxième nb svp :");
            int n2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("quelle opération '+' ou '*' :");
            char choix = (char)Console.Read();
            switch (choix)
            {
                case '*':
                    {
                        break;
                    }
                case '+':
                    {
                        c = Calcul.somme;
                        break;
                    }
            }
            result = c(n1, n2);
            Console.WriteLine("resultat :" + result.ToString());

            /*partie3*/
            string s = "voiture";
            Console.WriteLine(s.retireCar('i'));

        }

        public static int somme(int x)
        {
            Console.WriteLine("somme");
            return x + x;
        }
        public static int produit(int x)
        {
            Console.WriteLine("produit");
            return x * x;
        }
    }


    public static partial class Extensions
    {
        public static string cryptage(this string ch)
        {
            return ch + "hello";
        }
    }

}
