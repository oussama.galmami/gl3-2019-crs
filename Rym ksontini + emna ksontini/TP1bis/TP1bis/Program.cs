﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1bis
{
    static class extension
    {
        public static ArrayList filtre(this ArrayList a, Func<dynamic, bool> f)
        {
            ArrayList des = new ArrayList();
            foreach (dynamic x in a)
                if (f((dynamic)x))
                    des.Add(x);
            return des;
        }
    }

    public delegate void DelegateIn();
    class Personne
    {
        public Personne(string nom, int age) { this.nom = nom; this.age = age; }
        public override string ToString() { return this.nom + " " + this.age.ToString(); }
        public string nom;
        public int age;
    }

    class Pile<T> : System.Collections.IEnumerable
    {
        public event DelegateIn delegateInput;

        public Pile(int n) { this.mesT = new T[n]; this.nbElements = 0; }
        public void depile()
        {
            if (this.nbElements > 0) this.nbElements--;
        }
        private bool estPLeine()
        {
            return this.mesT.Length == this.nbElements;
        }
        public void empile(T unT)
        {
            if (!this.estPLeine())
            {
                this.mesT[this.nbElements] = unT;
                this.nbElements++;
            }
            else
            {
                delegateInput();
            }
        }
        public T leT(int n)
        {
            return this.mesT[n];
        }

        public IEnumerator GetEnumerator()
        {
            for (int i = mesT.Length - 1; i >= 0; i--)
                yield return mesT[i];
        }

        public void personne_empilee()
        {
            Console.WriteLine("nombre d elements:{0}", nbElements);
            Console.WriteLine("attention pile pleine");
        }

        private T[] mesT;
        private int nbElements;
    }
    class Program
    {
        static void Main(string[] args)
        {
            /*partie 1*/
            ArrayList desInts = new ArrayList();
            for (int i = 1; i <= 100; i++) { desInts.Add(i); }
            affiche(desInts.filtre((a) => a % 7 == 0));
            ArrayList capitales = new ArrayList() { "Paris", "Madrid", "Londres", "Rome", "Genève", "Dublin", "Moscou", "Zurich", "Prague" };
            affiche(filtre(capitales, (a) => a[0] == 'P' || a.Substring(0, 2).Equals("Ma") || a.Substring(a.Length - 2, 2).Equals("ve") || a[a.Length - 1] == 'e' || a.IndexOf("dr") != -1 || a.IndexOf("o") == -1));

            /*partie 2*/
            List<Personne> personnes = new List<Personne> { new Personne("Dupond", 15), new Personne("Durand", 25), new Personne("Rami", 18), new Personne("Paton", 24), new Personne("Harvis", 11), new Personne("Breton", 32), new Personne("Perdrix", 19), new Personne("Rapport", 19), new Personne("Boissette", 28), new Personne("Pamelle", 45), new Personne("Fétard", 15) };
            var toName = (from personne in personnes
                          where (personne.nom.IndexOf("to") != -1)
                          select personne.nom);
            foreach (var name in toName)
            {
                Console.WriteLine("{0}", name);
            }
            var ageMoy = (from personne in personnes
                          where (personne.age == (from per in personnes
                                                  select per.age).Average())
                          select personne.nom);
            foreach (var age in ageMoy)
            {
                Console.WriteLine("{0}", age);
            }
            Console.WriteLine("personne plus agee");
            var personnePlusAgee = (from personne in personnes
                                    where (personne.age == (from per in personnes
                                                            select per.age).Max())
                                    select personne);
            foreach (var age in personnePlusAgee)
            {
                Console.WriteLine("{0},{1}", age.nom, age.age);
            }

            Console.WriteLine("personne nom plus court");
            var personneNomCourt = (from personne in personnes
                                    where (personne.nom.Length == (from per in personnes
                                                                   select per.nom.Length).Min())
                                    select personne

                        );
            foreach (var court in personneNomCourt)
            {
                Console.WriteLine("{0},{1}", court.nom, court.age);
            }

            /*partie 3*/
            Pile<Personne> p = new Pile<Personne>(4);
            DelegateIn delegateIn = p.personne_empilee;
            delegateIn += aLEmpilement;
            p.delegateInput += delegateIn;
            p.depile();
            p.empile(new Personne("toto", 12));
            p.empile(new Personne("titi", 15));
            p.empile(new Personne("tutu", 25));
            p.empile(new Personne("toutou", 28));
            p.empile(new Personne("tintin", 14));
            p.empile(new Personne("tata", 11));
            p.depile();
            p.empile(new Personne("ota", 13));
            foreach (Personne pe in p)
                Console.WriteLine(pe.ToString());
        }
        static void affiche(ArrayList a)
        {
            foreach (var i in a)
                Console.WriteLine(i.ToString());
        }

        static ArrayList filtre(ArrayList a, Func<dynamic, bool> f)
        {
            ArrayList des = new ArrayList();
            foreach (dynamic x in a)
                if (f((dynamic)x))
                    des.Add(x);
            return des;
        }

        public static void aLEmpilement()
        {
            Console.WriteLine("hello");
        }
    }
}
