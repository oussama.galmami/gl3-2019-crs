﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Activities.Activity4
{
    class Personne
    {
        public string nom { get; set; }
        public int age { get; set; }
        public Personne(string nom, int age)
        {
            this.nom = nom;
            this.age = age;
        }
        public override string ToString()
        {
            return $"{nom} - {age}";
        }
    }

    class Activity4
    {
        static List<Personne> personnes = new List<Personne>{new Personne("Dupond",15), new
Personne("Durand",25),new Personne("Rami",18), new Personne("Paton",24),new
Personne("Harvis",11),new Personne("Breton",32),new Personne("Perdrix",19),
new Personne("Rapport",19),new Personne("Boissette",28),new Personne("Pamelle",45),
new Personne("Fétard",15)};

        public static void Main2()
        {
            // A
            List<Personne> people = (from personne in personnes
                                     where personne.nom.Contains("to")
                                     select personne).ToList();
            people.ForEach(p => Console.WriteLine(p));

            // B
            double a = (from personne in personnes
                        select personne.age).Average();
            Console.WriteLine(a);

            // C
            people = (from personne in personnes
                                     where personne.age == (from p in personnes select p.age).Max()
                                     select personne).ToList();
            people.ForEach(p => Console.WriteLine(p));

            // D
            people = (from personne in personnes
                      where personne.nom.Length == (from p in personnes select p.nom.Length).Min()
                      select personne).ToList();
            people.ForEach(p => Console.WriteLine(p));

            Console.ReadKey();
        }
    }
}
