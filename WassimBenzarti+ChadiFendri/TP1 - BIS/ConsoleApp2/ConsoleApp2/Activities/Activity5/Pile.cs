﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Activities.Activity5
{
    class Pile<T>:IEnumerable
    {
        private T[] mesT;
        private int nbElements;

        public delegate void DelegateOfFull();
        public event DelegateOfFull myEvent;
        public Pile(int n)
        {
            this.mesT = new T[n];
            this.nbElements = 0;
            myEvent = () => Console.WriteLine("nombre d'element est " + nbElements);
        }
        public void depile()
        {
            if (this.nbElements > 0)
                this.nbElements--;
        }
        private bool estPLeine()
        {
            return this.mesT.Length == this.nbElements;
        }
        public void empile(T unT)
        {
            if (!this.estPLeine())
            {
                this.mesT[this.nbElements] = unT;
                this.nbElements++;
            }
            
            if (this.estPLeine())
            {
                myEvent?.Invoke();
            }
        }
        public T leT(int n)
        {
            return this.mesT[n];
        }

        public IEnumerator GetEnumerator()
        {
            for(int index = this.nbElements-1; index >= 0; index--)
            {
                yield return this.mesT[index];
            }
        }
    }
}
