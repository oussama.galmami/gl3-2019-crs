﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Activities.Activity5
{
    class Personne
    {
        private int age;
        private string nom;
        public Personne(string nom, int age)
        { this.nom = nom; this.age = age; }
        public override string ToString()
        {
            return this.nom + " " + this.age.ToString();
        }
        
    }
}
