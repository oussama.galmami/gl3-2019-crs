﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Activities
{
    class Calcul
    {
        public static int somme(int a, int b)
        {
            return a + b;
        }
        public static int produit(int a, int b)
        {
            return a * b;
        }
    }
    class Program
    {
        public delegate int CalculatorDelegator(int a, int b);

        static void Main3(string[] args)
        {
            Console.WriteLine("premier nb svp :");
            int n1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("deuxième nb svp :");
            int n2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("quelle opération '+' ou '*' :");
            char choix = (char)Console.Read();

            CalculatorDelegator traitement = choix.Equals("+") ? (CalculatorDelegator)Calcul.somme: Calcul.produit;

            Console.WriteLine("resultat :" + traitement(n1,n2).ToString());
            Console.ReadKey();
        }
    }
}

