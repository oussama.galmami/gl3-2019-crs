﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Activities.Activity3
{
    class Activity3
    {
        static void Main3(string[] args)
        {
            // Activity 3.1
            ArrayList desInts = new ArrayList();
            for (int i = 1; i <= 100; i++)
            {
                desInts.Add(i);
            }
            affiche<int>(desInts.filtre<int>(a => a % 7 == 0));

            // Activity 3.2
            ArrayList capitales = new ArrayList(){"Paris","Madrid","Londres","Rome","Genève","Dublin","Moscou","Zurich","Prague" };
            affiche<string>(
                filtre(capitales, a => a.StartsWith("P")
                || a.StartsWith("Ma")
                || a.EndsWith("e")
                || a.Contains("dr")
                || !a.Contains("o")
                )
            );

            // Activity 3.3
            capitales = new ArrayList() { "Paris", "Madrid", "Londres", "Rome", "Genève", "Dublin", "Moscou", "Zurich", "Prague" };
            affiche<string>(
                filtre<string>(capitales, a => a.StartsWith("P")
                || a.StartsWith("Ma")
                || a.EndsWith("e")
                || a.Contains("dr")
                || !a.Contains("o")
                )
            );

            Console.ReadKey();

        }
        static ArrayList filtre(ArrayList ar, Func<string, bool> f)
        {
            ArrayList lesInts = new ArrayList();
            foreach (string i in ar)
            {
                if (f((string)i))
                {
                    lesInts.Add(i);
                }
            }
            return lesInts;
        }

        static ArrayList filtre<T>(ArrayList ar, Func<T, bool> f)
        {
            ArrayList lesInts = new ArrayList();
            foreach (T i in ar)
            {
                if (f((T)i))
                {
                    lesInts.Add(i);
                }
            }
            return lesInts;
        }
        static void affiche<T>(ArrayList ar)
        {
            foreach (T i in ar)
            {
                Console.WriteLine(i.ToString());
            }
        }

    }
}
