﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2.Activities.Activity3
{
    public static class Extensions
    {
        public static ArrayList filtre<T>(this ArrayList ar, Func<T, bool>f)
        {
            ArrayList lesInts = new ArrayList();
            foreach (T i in ar)
            {
                if(f(i))
                {
                    lesInts.Add(i);
                }
            }
            return lesInts;
        }
    }
}
