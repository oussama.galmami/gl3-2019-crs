using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.Activity1;

namespace ConsoleApp1
{
    class Program
    {

        delegate Double MyIntTransformer(int i);

        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            string a = "ABC";
            //Console.WriteLine("The string is "+ (a.isAllUpperCase() ? "": " not") + "uppercased");

            MyIntTransformer intTransformer = x => Math.Pow(x, 4);
            double result = intTransformer(5);
            //Console.WriteLine(result); //625

            Student student = new Student { name = "Wassim", age = 22 };
            //Console.WriteLine(student);

            //var message = new { objet = "Mon objet", contenu = "Bonjour" };

            int[] array = new int[] { 1,2,3,4,5,6,7,8,9 };

            var query = from num in array
                        where (num % 2 == 0)
                        select num;

            foreach(int num in query)
            {
                Console.WriteLine("{0}", num);
            }

            



            Console.ReadKey();
            

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
        }
    }
}
