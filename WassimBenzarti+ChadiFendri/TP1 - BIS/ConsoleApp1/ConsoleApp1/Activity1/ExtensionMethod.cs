﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Activity1
{
    public static partial class StringExtensions
    {
        public static bool isAllUpperCase(this string s)
        {
            return s.ToUpper().Equals(s);
        }
    }
}
