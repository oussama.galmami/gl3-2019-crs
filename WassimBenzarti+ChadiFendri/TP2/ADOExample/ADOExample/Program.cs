using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOExample
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ADOExample;Integrated Security=True;Pooling=False";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    /*
                        SqlCommand insertCommand = new SqlCommand(
                        "INSERT INTO Etudiant(nom,prenom,email,age) VALUES(@name, @prenom, @email, @age)",
                        connection);
                        insertCommand.Parameters.AddWithValue("@name", "Benz");
                        insertCommand.Parameters.AddWithValue("@prenom", "Wassim");
                        insertCommand.Parameters.AddWithValue("@email", "wass@mail.com");
                        insertCommand.Parameters.AddWithValue("@age", 12);

                        insertCommand.ExecuteNonQuery();
*/
                    // SQL Command
                    SqlCommand filterCommand = connection.CreateCommand();
                    filterCommand.CommandText =
                        "SELECT * from Etudiant WHERE Etudiant.age = @age";
                    filterCommand.Parameters.AddWithValue("@age", 90);
                    SqlDataReader reader = filterCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine($"{reader["nom"]} {reader["prenom"]} {reader["age"]}  ");
                    }

                    //Procedure stocké
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    connection.Close();
                }

            }


            Console.ReadKey();

        }
    }
}
