ï»¿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_Csharp_n2
{
    using Personne;

    class Pile<T>
    {
        public Pile(int n)
        {
            this.mesT = new T[n];
            this.nbElements = 0;
        }

        public void depile()
        {
            if (this.nbElements > 0) this.nbElements--;
        }

        private bool estPLeine()
        {
            return this.mesT.Length == this.nbElements;
        }

        public void empile(T unT)
        {
            if (!this.estPLeine())
            {
                this.mesT[this.nbElements] = unT;
                this.nbElements++;
            }
        }

        public T leT(int n)
        {
            return this.mesT[n];
        }

        private T[] mesT;
        private int nbElements;

    }

    class Act5
    {
        /*static void Main(string[] args)
        {
            Pile<Personne> p = new Pile<Personne>(4);
            p.depile();
            p.empile(new Personne("toto", 12));
            p.empile(new Personne("titi", 15));
            p.empile(new Personne("tutu", 25));
            p.depile();
            p.empile(new Personne("toutou", 28));
            p.empile(new Personne("tintin", 14));
            p.empile(new Personne("tata", 11));
            
            foreach (Personne pe in p.mesT)
                Console.WriteLine(pe.ToString());
        }*/
    }

}

