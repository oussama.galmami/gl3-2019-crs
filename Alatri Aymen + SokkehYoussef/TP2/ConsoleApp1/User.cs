ï»¿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    public class User
    {
        [Key]
        public string Id { get; set; }
        [ConcurrencyCheck]
        public string Username { get; set; }
        [MinLength(8)]
        public string Password { get; set; }
    }

