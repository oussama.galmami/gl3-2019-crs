ï»¿using System;
using System.ComponentModel.DataAnnotations;

public class Company
{
    [Key]
    public string CompanyId
    {
        get;
        set;
    }
    [StringLength(20)]
    public string CompanyName
    {
        get;
        set;
    }
}