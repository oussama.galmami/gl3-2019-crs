ï»¿

namespace AdoNetConsoleApplication{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    class Program
    {
        public SqlConnection Db_Connection()
        {            SqlConnection con = null;
            try {
                con = new SqlConnection("data source=ALATRIAYMEN\\SQLEXPRESS; database=TP_C#; integrated security=true");
                con.Open();
                Console.WriteLine("Connexion est ouvert avec succÃ©s");
                return con;
               
            }
            catch(Exception e) {
                Console.WriteLine(e);
                return null;
            }
        }

        public void CreateTable(SqlConnection con)
        { 
            try {
                SqlCommand cm = new SqlCommand("CREATE TABLE Personne (id int not null IDENTITY(1,1), nom varchar(100),prenom varchar(100),email varchar(50),PRIMARY KEY(id))", con);   
                int res = cm.ExecuteNonQuery();
                Console.WriteLine(res+" Table crÃ©e avec succÃ©es");
            }
            catch (Exception e) {
                Console.WriteLine("OOPs, something went wrong." + e);
            }
            // Closing the connection  
            finally
            {
                con.Close();
            }
        }

        public void Insert_Personne(SqlConnection con, string nom,string prenom, string email)
        {
            SqlCommand cm = new SqlCommand("INSERT INTO Personne VALUES('"+nom+"','"+prenom+"','"+email+"')",con);
            int res = cm.ExecuteNonQuery();
            Console.WriteLine(res);
        }

        public void Delete_Personne(SqlConnection con,int id)
        {
            SqlCommand cm = new SqlCommand("DELETE FROM Personne WHERE id = "+id, con);
            int res = cm.ExecuteNonQuery();
            Console.WriteLine(res);
        }

        public void Affiche_Personnes(SqlConnection con)
        {
            SqlCommand cm = new SqlCommand("SELECT * FROM Personne", con);
            SqlDataReader sdr = cm.ExecuteReader();

            while (sdr.Read())
            {
                Console.WriteLine(sdr["id"] + " " + sdr["nom"] + " " + sdr["prenom"] + " " + sdr["email"]);
            }
        }

        //Exemple d'appel d'un procÃ©dure stockÃ©

        public void Affiche_Personne_ProcS(SqlConnection con,int id)
        {
            SqlCommand CMD = new SqlCommand("SQL_Aff", con);
            CMD.CommandType = CommandType.StoredProcedure;
            CMD.Parameters.Add("@id",SqlDbType.Int).Value=id;
            SqlDataReader sdr = CMD.ExecuteReader();
            while (sdr.Read())
            {
                Console.WriteLine(sdr["id"] + " " + sdr["nom"] + " " + sdr["prenom"] + " " + sdr["email"]);
            }
        }

        //exemple : le mode de programmation dÃ©connectÃ©

        public void Affiche_mode_dec(SqlConnection con)
        {
            //DataTable Personne = new DataTable();
            DataSet source_commande = new DataSet();
            //source_commande.Tables.Add(Personne);
            SqlDataAdapter adapter_cmd = new SqlDataAdapter("SELECT * FROM Personne", con);
            adapter_cmd.Fill(source_commande, "Personne");
            foreach(DataRow row in source_commande.Tables["Personne"].Rows)
            {
                Console.WriteLine(row[0]+" "+row[1]+" "+row[2]);
            }
  
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            SqlConnection con = p.Db_Connection();
            //p.CreateTable(con);
            //p.Insert_Personne(con, "aymen", "alatri", "aymenalatri1@gmail.com");
            //p.Insert_Personne(con, "youssef", "sokkeh", "youssefsokkeh1@gmail.com");
            // p.Delete_Personne(con,2);
            p.Affiche_mode_dec(con);

        }
    }
}
